import { NgxTreeAppPage } from './app.po';

describe('ngx-tree-app App', () => {
  let page: NgxTreeAppPage;

  beforeEach(() => {
    page = new NgxTreeAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
