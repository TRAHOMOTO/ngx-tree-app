# Technologies: Angular

##The task:
1.	Create application for a virtual folders/files tree (no actual folders/files should be created).
2.	Each folder/file except root can be renamed or removed.
3.	App state should be saved in localStorage.
4.	Any tree changes should also be saved in localStorage.
5.	Clicking on a file will display it’s content in a column on the right.

![example](https://image.prntscr.com/image/CyREcLtwQOS3zROYsYCMsg.png)

##Bonus:
1.	Add payment capabilities
  -	Add another button next to the root called “Pay”
  - Each file now has a price. Save and display however you like.
  - Pressing the button will generate a (testing) payment page (hosted by PayMe), by calling the following API:

https://preprod.paymeservice.com/api/generate-sale

```javascript
 {
    "seller_payme_id":"MPL14985-68544Z1G-SPV5WK2K-0WJWHC7N",
    "sale_price":"10000",  // Sum price of all selected files, in cents
    "currency":"USD",
    "product_name": "Payment for files",
    "installments":"1",
    "language": "en"
 }
```
_*The only parameter you need to change in the request above is the “sale_price”._

**Example response:**
```javascript
{
  "status_code": 0,
  "sale_url":"https://preprod.paymeservice.com/sale/generate/SALE1498-567890BD-XKYB7QH5-C3MQXRKB",
  "payme_sale_id": "SALE1498-567890BD-XKYB7QH5-C3MQXRKB",
  "payme_sale_code": 1234567,
  "price": 10000,
  "transaction_id": "",
  "currency": "USD"
}
```

_*Successful API result is when "status_code": 0_

_**The payment page URL is located in “sale_url” parameter._

- Open the payment page URL you received from the API, in an IFRAME.

2.	Any other cool features will be a plus!

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
