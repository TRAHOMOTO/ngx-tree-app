// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  PayMe: {
    url: 'https://preprod.paymeservice.com/api/generate-sale',
    seller_payme_id: 'MPL14985-68544Z1G-SPV5WK2K-0WJWHC7N'
  }
};
