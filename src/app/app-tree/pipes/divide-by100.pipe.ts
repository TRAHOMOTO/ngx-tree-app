import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'divideBy100'
})
export class DivideBy100Pipe implements PipeTransform {

  transform(value: any): any {
    const valueNumber = Number(value);
    if ( isNaN(valueNumber) ) {
      return value;
    } else {
      return value/100;
    }
  }

}
