import {TreeNode} from './model/tree-node';

export function nodesHasPrice(nodes: TreeNode[] ) {
  return nodes.filter( node => !!node.price );
}

export function nodesTotalPrice( nodes: TreeNode[] ) {
  return nodes.reduce( (acc, node) => acc += node.price, 0 );
}
