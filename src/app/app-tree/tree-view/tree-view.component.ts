import {Component, Input, ViewEncapsulation} from '@angular/core';
import {TreeNode} from '../model/tree-node';
import {TreeService} from '../services/tree.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-tree-view',
  templateUrl: './tree-view.component.html',
  encapsulation: ViewEncapsulation.None
})
export class TreeViewComponent {

  @Input() children: TreeNode[];

  constructor(private treeService: TreeService,
              private router: Router) {}

  onCheck(node: TreeNode) {
    node.checked = !node.checked;
    this.checkRecursive(node, node.checked);
  }

  onAddDirTo(parentNode: TreeNode) {
    this.router.navigate(['add', 'directory', parentNode.id]);
  }

  onAddFileTo(parentNode: TreeNode) {
    this.router.navigate(['add', 'file', parentNode.id]);
  }


  onUpdated(node: TreeNode) {
    this.treeService
      .replaceNode(node.id, node)
      .subscribe(() => {}, console.error);
  }

  private checkRecursive(node: TreeNode, state: boolean): void {
    this.treeService.checked(node);
    node.children.forEach(child => {
      child.checked = state;
      this.checkRecursive(child, state);
    });
  }
}
