export abstract class TreeNode {
  isEdit = false;

  constructor(public id: string,
              public name: string,
              public price: number,
              public children: TreeNode[],
              public parentId?: string,
              public expanded = false,
              public checked = false) {
    this.setParent();
  }

  abstract get isLeaf(): boolean;
  abstract get isContainer(): boolean;

  toggle(): void {
    this.expanded = !this.expanded;
  }

  editStart() {
    this.isEdit = true;
  }

  editEnd() {
    this.isEdit = false;
  }

  addChild(node: TreeNode) {
    this.children.push(node);
  }

  private setParent() {
    this.children.forEach( c => c.parentId = this.id );
  }

  abstract toJSON();
}
