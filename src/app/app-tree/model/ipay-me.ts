export interface IPayMeRequest {
  seller_payme_id: string;
  sale_price: number;
  currency: string;
  product_name: string;
  installments: string;
  language: string;
}

export interface IPayMeResponse {
  status_code: number;
  sale_url: string;
  payme_sale_id: string;
  payme_sale_code: number;
  price: number;
  transaction_id: string;
  currency: string;
}
