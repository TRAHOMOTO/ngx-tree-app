import {TreeNode} from './tree-node';

export class File extends TreeNode {

  static fromJSON( json: any ): TreeNode {
    const {id, name, price, content, parentId, checked} = json;
    return new File(id, name, +price, content, parentId, checked);
  }

  constructor(id: string,
              name: string,
              price: number,
              public content: string,
              parentId = null,
              checked = false) {

    super(id, name, price, [], parentId, false, checked);
  }

  get isLeaf(): boolean {
    return true;
  }

  get isContainer(): boolean {
    return false;
  }

  toJSON() {
    const json = Object.assign({}, this);
    delete json.children;
    delete json.expanded;
    delete json.isEdit;
    return json;
  }
}
