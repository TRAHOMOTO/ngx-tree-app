export class FileContent<K, T> {
  constructor(public id: K, public content: T) {}
}
