import {TreeNode} from './tree-node';

export class Directory extends TreeNode {

  static fromJSON( json: any ): TreeNode {
    const {id, name, content, parentId, expanded, checked} = json;
    return new Directory(id, name, [], parentId, expanded, checked);
  }

  constructor(id: string,
              name: string,
              children: TreeNode[],
              parentId = null,
              expanded = false,
              checked = false) {

    super(id, name, null, children, parentId, expanded, checked);
  }

  get isLeaf(): boolean {
    return !!this.children.length;
  }

  get isContainer(): boolean {
    return true;
  }

  toJSON() {
    const json: any = Object.assign({}, this);
    json.children = json.children.map(node => node.id);
    delete json.isEdit;
    return json;
  }

}
