import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/distinctUntilChanged';

import {nodesHasPrice, nodesTotalPrice} from '../app-tree.utils';
import {TreeService} from '../services/tree.service';
import {TreeNode} from '../model/tree-node';

@Component({
  selector: 'app-tree-home',
  templateUrl: './tree-home.component.html',
  styleUrls: ['./tree-home.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TreeHomeComponent implements OnInit {

  root$: Observable<TreeNode[]>;
  totalAmount$: Observable<number>;
  checked$: Observable<TreeNode[]>;

  constructor(private treeService: TreeService,
              private router: Router) {}

  ngOnInit() {
    this.root$ = this.treeService.loadTree();
    this.checked$ = this.treeService.selected$;
    this.totalAmount$ = this.treeService.selected$
      .map( nodesHasPrice )
      .map( nodesTotalPrice );
  }

  addDirectory() {
    this.router.navigate([ 'add', 'directory', 'root']);
  }

  addFile() {
    this.router.navigate([ 'add', 'file', 'root']);
  }

  populateTree() {
    this.root$ = this.treeService.populateTree();
  }

  removeSeleted() {
    const selectedIds = this.treeService.getSelected().map(n => n.id);
    this.router.navigate([ 'remove-selected', JSON.stringify(selectedIds) ]);
  }


}
