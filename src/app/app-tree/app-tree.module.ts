import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {LocalStorageModule} from 'angular-2-local-storage';
import {HttpClientModule} from '@angular/common/http';

import { AppTreeRoutingModule } from './app-tree-routing.module';
import {TreeHomeComponent} from './tree-home/tree-home.component';
import { TreeViewComponent } from './tree-view/tree-view.component';
import { TreeNodeComponent } from './tree-node/tree-node.component';
import {TreeService} from './services/tree.service';
import { ConfirmComponent } from './confirm/confirm.component';
import { FileContentComponent } from './file-content/file-content.component';
import { CheckoutComponent } from './checkout/checkout.component';
import {PayMeResolver} from './services/pay-me.resolver';
import {DivideBy100Pipe} from './pipes/divide-by100.pipe';
import { ConfirmRemoveSelectedComponent } from './confirm-remove-selected/confirm-remove-selected.component';
import { EditDirectoryComponent } from './edit-directory/edit-directory.component';
import { TreeInlineEditComponent } from './tree-inline-edit/tree-inline-edit.component';
import {EditFileComponent} from './edit-file/edit-file.component';
import {SafeUrlPipe} from './pipes/safe-url.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,

    LocalStorageModule.withConfig({
      prefix: 'ngx-tree-app',
      storageType: 'localStorage'
    }),

    AppTreeRoutingModule
  ],
  exports: [ RouterModule ],
  declarations: [
    DivideBy100Pipe,
    SafeUrlPipe,

    TreeHomeComponent,
    TreeViewComponent,
    TreeNodeComponent,
    ConfirmComponent,
    FileContentComponent,
    CheckoutComponent,
    ConfirmRemoveSelectedComponent,
    EditDirectoryComponent,
    EditFileComponent,
    TreeInlineEditComponent
  ],
  providers: [
    TreeService, PayMeResolver
  ]
})
export class AppTreeModule { }
