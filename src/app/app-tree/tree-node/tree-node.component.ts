import {Component, EventEmitter, Input, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {TreeNode} from '../model/tree-node';
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-tree-node',
  templateUrl: './tree-node.component.html',
  styleUrls: ['./tree-node.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TreeNodeComponent {

  @Input() node: TreeNode;
  @Output() updated = new EventEmitter<TreeNode>();

  onToggle() {
    this.node.toggle();
    this.updated.emit(this.node);
  }

  onSubmit( ): void {
    this.updated.emit(this.node);
  }
}
