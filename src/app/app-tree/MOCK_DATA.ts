import {Directory} from './model/directory';
import {File} from './model/file';
import {FileContent} from './model/file-content';

import * as nanoId from 'nanoid';

const lorem1 = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam non porttitor nulla, et tincidunt metus. Sed porttitor volutpat elit, non aliquet elit vestibulum id. Integer porttitor velit massa, a aliquet velit dictum sed. Donec eros massa, iaculis non arcu a, tincidunt commodo nulla. Suspendisse potenti. Ut fermentum blandit fringilla. Nulla a eros lacus. Maecenas convallis sapien ac pharetra euismod. Vivamus nulla erat, ullamcorper sed rhoncus a, sollicitudin sed augue. Fusce interdum placerat arcu ac consequat. Nam sed metus pretium, tincidunt diam a, fringilla ante. Nulla scelerisque orci ac tempus vehicula. Praesent dignissim nulla sit amet tempor sollicitudin. Ut sagittis condimentum molestie.';
const lorem2 = 'Cras at feugiat arcu, scelerisque porta leo. Etiam consequat magna massa, at lacinia urna lobortis eu. Integer nisl risus, efficitur eget pellentesque ut, tincidunt in libero. In eu sodales dolor. Vestibulum vitae lorem venenatis, mattis erat et, iaculis dui. Nulla nec lectus in tellus consectetur volutpat id vel diam. Curabitur a pharetra metus, id porttitor mauris. Morbi diam lacus, luctus ac auctor a, scelerisque id turpis. Curabitur at tempus sapien. Vivamus commodo pretium libero. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur eu molestie leo.';
const lorem3 = 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?';

const lorem4 = 'Vestibulum sit amet ligula orci. Phasellus convallis convallis enim, ut cursus libero dapibus vel. Duis in velit commodo arcu volutpat gravida sit amet non justo. Curabitur pretium laoreet eros, ut ornare tellus ultricies non. Vestibulum a erat vitae dolor tempus maximus. Proin viverra mi neque, et porta tortor ultrices at. Donec facilisis dolor lacus, sed faucibus dui volutpat ac. Maecenas magna purus, posuere sed posuere et, condimentum id ipsum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras at orci vitae ex mollis porttitor vel vel mi.';
const lorem5 = 'Praesent feugiat at augue cursus rutrum. Sed ac magna ultrices, congue mi sed, faucibus purus. Etiam blandit nibh risus, vel mollis dui scelerisque nec. Duis ligula mauris, vehicula in semper sit amet, lacinia id risus. Proin finibus orci a enim blandit ornare. Nam turpis turpis, fermentum in semper laoreet, ullamcorper in ipsum. Phasellus metus leo, facilisis sed fringilla non, ornare vitae nisl. Morbi laoreet, quam et maximus pretium, est tortor posuere diam, in gravida erat nisi sit amet quam. Curabitur eget ipsum eu nibh convallis tempor at non turpis. Proin vel sollicitudin arcu, at malesuada elit.';
const lorem6 = 'Phasellus ultrices erat ut nisl aliquam, quis pellentesque eros venenatis. Donec maximus odio at ipsum pharetra, vel pharetra odio blandit. Vestibulum aliquam volutpat dui in blandit. Donec vel elit sit amet orci blandit congue. In sit amet neque egestas, egestas lorem sed, posuere dui. Suspendisse potenti. Nullam molestie velit sit amet dolor porttitor ullamcorper. Cras nibh est, tempor a arcu id, euismod mattis augue. Maecenas convallis elit sit amet tortor placerat pharetra. Nam lacinia nisl id tincidunt condimentum.';

const lorem7 = 'Fusce in posuere nisi. Aliquam auctor nulla lacus, eget pulvinar quam mollis pretium. Etiam cursus lectus sed est suscipit, non interdum nibh lobortis. Maecenas in turpis odio. Morbi enim mauris, porttitor sit amet imperdiet a, vestibulum in orci. Sed eu fringilla diam. Curabitur ut neque accumsan felis lacinia laoreet. Etiam et nisl vitae sapien venenatis sagittis semper ac purus. Nunc ultrices massa metus, quis tincidunt enim elementum sit amet.';
const lorem8 = 'Suspendisse rhoncus dui pulvinar orci rhoncus maximus. Etiam tincidunt leo quis turpis finibus viverra. Aenean dolor neque, dictum et tristique sit amet, accumsan sit amet neque. Vivamus vel dolor non urna cursus vehicula. Aliquam erat volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nullam ipsum magna, pellentesque et ultricies id, venenatis tristique nunc. Fusce quis leo nec justo varius consequat nec vitae est. Aliquam quis neque quis metus facilisis vehicula. Donec elementum, eros ut venenatis porta, nunc massa convallis nunc, id tristique neque urna semper nulla. Vivamus sit amet mi vitae ante ultricies accumsan id sed massa. Fusce in pulvinar augue. Praesent pellentesque luctus dolor non venenatis. In hac habitasse platea dictumst. Suspendisse finibus et quam at gravida. Cras ultrices interdum risus.';
const lorem9 = 'Nam dignissim dignissim erat id finibus. Nulla sed augue id erat euismod interdum. Curabitur congue ipsum sit amet sapien consequat commodo. Nullam elementum, eros et facilisis efficitur, nibh nibh suscipit urna, quis laoreet lacus libero ut mauris. Nullam at risus neque. Morbi hendrerit sagittis est, et accumsan ligula auctor id. Fusce porttitor tristique purus, vel condimentum elit iaculis eu. Phasellus volutpat, mauris non fermentum lacinia, dolor sem iaculis odio, sit amet gravida nibh risus quis ex. Phasellus vitae libero eget nunc euismod placerat. Aliquam accumsan imperdiet metus, at hendrerit justo porta sed.';

const lorem10 = 'Sed viverra consectetur tincidunt. In vestibulum elit arcu, ut pellentesque lorem congue egestas. Duis dignissim vehicula tortor rhoncus sollicitudin. Phasellus eleifend pulvinar lacus ut consequat. Phasellus sollicitudin turpis fermentum viverra mollis. Curabitur gravida velit massa, ac dignissim arcu imperdiet eu. Nulla molestie enim efficitur ipsum fringilla ornare ut eget sem. Aenean imperdiet ipsum nec aliquam tristique. Morbi in pulvinar nunc. Cras efficitur diam mauris. Proin eget leo a lacus scelerisque ornare eget eu nunc. Praesent euismod in magna accumsan vulputate. Suspendisse hendrerit nibh ac justo varius, in lobortis sapien ultricies. Cras ultrices quam in accumsan tempor. Aliquam faucibus et ipsum vel sodales. Curabitur congue dolor mi, vel tincidunt velit suscipit at.';
const lorem11 = 'Sed tempus posuere est nec egestas. Pellentesque vel magna sed nunc ullamcorper fringilla eget et nisl. Praesent pharetra id tortor vel accumsan. Nullam elit odio, ultrices id lectus ac, commodo pretium enim. Curabitur vel lacinia elit. In hendrerit vitae dui vitae gravida. Nulla facilisi. Integer ornare libero non aliquam malesuada. Vivamus dapibus dignissim justo, vitae consectetur lectus dignissim a.';
const lorem12 = 'Nulla facilisi. Praesent sit amet justo vitae est auctor blandit a at elit. Donec eu egestas ex, eget sagittis nunc. Maecenas pellentesque, risus quis elementum sollicitudin, urna urna facilisis sapien, quis vehicula nunc elit ut lorem. Curabitur gravida sollicitudin ante, eu accumsan leo finibus vitae. Fusce cursus eleifend diam et bibendum. In et tortor justo. Quisque sem lectus, sagittis eu dolor vel, ullamcorper interdum urna. Nulla mattis ex tortor, eget vehicula magna ullamcorper non. Sed eu ex dolor. Maecenas lobortis finibus mi, et vulputate est feugiat eget. Suspendisse potenti. In commodo et nulla ut finibus. Etiam vehicula vel purus quis sagittis. Mauris efficitur luctus nunc, non viverra justo dignissim id. Curabitur id rhoncus tellus.';

// ---------------------------------------------------------------------

const content1 = `content:${nanoId()}`;
const content2 = `content:${nanoId()}`;
const content3 = `content:${nanoId()}`;
const content4 = `content:${nanoId()}`;
const content5 = `content:${nanoId()}`;
const content6 = `content:${nanoId()}`;
const content7 = `content:${nanoId()}`;
const content8 = `content:${nanoId()}`;
const content9 = `content:${nanoId()}`;
const content10 = `content:${nanoId()}`;
const content11 = `content:${nanoId()}`;

// ---------------------------------------------------------------------

const img1 = new File(`node:file:${nanoId()}`, 'image1.jpg', 1000, content1);
const img2 = new File(`node:file:${nanoId()}`, 'image2.jpg', 2000, content2);
const img3 = new File(`node:file:${nanoId()}`, 'image3.jpg', 3000, content3);

const img10 = new File(`node:file:${nanoId()}`, 'image10.jpg', 3999, content4);
const img20 = new File(`node:file:${nanoId()}`, 'image20.jpg', 5000, content5);
const img30 = new File(`node:file:${nanoId()}`, 'image30.jpg', 6000, content6);

const img11 = new File(`node:file:${nanoId()}`, 'image11.jpg', 7000, content7);
const img22 = new File(`node:file:${nanoId()}`, 'image22.jpg', 8000, content8);
const img33 = new File(`node:file:${nanoId()}`, 'image33.jpg', 9000, content9);

const song1 = new File(`node:file:${nanoId()}`, 'song1.mp3', 2000, content10);
const song2 = new File(`node:file:${nanoId()}`, 'song2.mp3', 3000, content11);

// ---------------------------------------------------------------------

const fall2014 = new Directory(`node:dir:${nanoId()}`, 'Fall 2014', [img1, img2, img3]);
const summer2014 = new Directory(`node:dir:${nanoId()}`, 'Summer 2014', [img10, img20, img30]);

const pics = new Directory(`node:dir:${nanoId()}`, 'Pictures', [summer2014, fall2014, img11, img22, img33]);
const music = new Directory(`node:dir:${nanoId()}`, 'Music', [song1, song2]);

// ---------------------------------------------------------------------

export const FILE_CONTENTS = [
  new FileContent<string, string>(content1, lorem1),
  new FileContent<string, string>(content2, lorem2),
  new FileContent<string, string>(content3, lorem3),

  new FileContent<string, string>(content4, lorem4),
  new FileContent<string, string>(content5, lorem5),
  new FileContent<string, string>(content6, lorem6),

  new FileContent<string, string>(content7, lorem7),
  new FileContent<string, string>(content8, lorem8),
  new FileContent<string, string>(content9, lorem9),

  new FileContent<string, string>(content10, lorem10),
  new FileContent<string, string>(content11, lorem11)
];

export const FILES = [
  img1, img2, img3,
  img10, img20, img30,
  img11, img22, img33,
  song1, song2
];

export const DIRS = [
  fall2014, summer2014, pics, music
];
