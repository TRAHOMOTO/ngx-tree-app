import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {TreeNode} from '../model/tree-node';


const ENTER = 13;
const ESC = 27;

@Component({
  selector: 'app-tree-inline-edit',
  templateUrl: './tree-inline-edit.component.html',
  styleUrls: ['./tree-inline-edit.component.css']
})
export class TreeInlineEditComponent implements OnInit {

  @Input() node: TreeNode;
  @Output() submit = new EventEmitter<TreeNode>();
  @ViewChild('editForm') editForm: NgForm;

  title: string;

  ngOnInit(): void {
    this.title = this.node.name;
  }

  onSubmit() {

    if (this.editForm.invalid) {
      return;
    }

    this.save();
  }

  onCancel() {
    this.cancel();
  }

  onKeyUp(ev: KeyboardEvent) {
    if (ev.keyCode === ENTER) { this.save(); }
    if (ev.keyCode === ESC) { this.cancel(); }
  }

  save() {

    const newName = this.editForm.value.title;

    this.node.name = newName;
    this.node.editEnd();
    this.submit.emit(this.node);
  }

  cancel() {
    this.editForm.resetForm();
    this.node.editEnd();
  }

}
