import {Component, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {TreeService} from '../services/tree.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.css']
})
export class ConfirmComponent implements OnDestroy {

  private subscr: Subscription;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private treeService: TreeService) { }

  ngOnDestroy() {
    if (this.subscr) {
      this.subscr.unsubscribe();
    }
  }

  removeNode() {
    const nodeId = this.route.snapshot.params['id'];
    this.subscr = this.treeService
      .removeBranch( nodeId )
      .subscribe( () => this.router.navigateByUrl('tree') );
  }

}
