import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {Observable} from 'rxjs/Observable';
import {IPayMeResponse} from '../model/ipay-me';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  pay$: Observable<IPayMeResponse>;
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.pay$ = this.route.data.pluck<any, IPayMeResponse>('pay');
  }
}
