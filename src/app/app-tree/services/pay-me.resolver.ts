import { Injectable } from '@angular/core';

import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {IPayMeRequest, IPayMeResponse} from '../model/ipay-me';
import {Observable} from 'rxjs/Observable';
import {TreeService} from './tree.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {nodesHasPrice, nodesTotalPrice} from '../app-tree.utils';
import 'rxjs/add/operator/publishLast';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/first';
import {environment} from '../../../environments/environment';

// TODO - add error handling

@Injectable()
export class PayMeResolver implements Resolve<IPayMeResponse> {

  static readonly PAY_ME_URL = environment.PayMe.url;
  static readonly HEADERS = {headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8') };

  constructor(private treeService: TreeService,
              private http: HttpClient,
              private router: Router) {}

  totalAmount(): number {
    const onlySelected = nodesHasPrice( this.treeService.getSelected() );
    return nodesTotalPrice( onlySelected );
  }

  postPayMe(params: IPayMeRequest): Observable<IPayMeResponse> {
    return this.http.post<IPayMeResponse>(
      PayMeResolver.PAY_ME_URL,
      params,
      PayMeResolver.HEADERS
    );
  }

  buildParams(sale_price: number): IPayMeRequest {
    return {
      seller_payme_id: environment.PayMe.seller_payme_id,
      sale_price: sale_price,
      currency: 'USD',
      product_name: 'Payment for files',
      installments: '1',
      language: 'en'
    };
  }

  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<IPayMeResponse> | Promise<IPayMeResponse> {

    const amount = this.totalAmount();
    if (amount > 0) {
      const params = this.buildParams(amount);
      return this.postPayMe(params).publishLast().refCount();
    } else {
      this.router.navigateByUrl('/tree');
    }
  }

}
