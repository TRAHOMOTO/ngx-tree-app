import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import * as _ from 'lodash';
import * as nanoId from 'nanoid';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/merge';

import {LocalStorageService} from 'angular-2-local-storage';

import * as MOCK_DATA from '../MOCK_DATA';
import {TreeNode} from '../model/tree-node';
import {File} from '../model/file';
import {Directory} from '../model/directory';
import {FileContent} from '../model/file-content';

// TODO - redesign this horror to redux like. And maybe split service to more simple services.

@Injectable()
export class TreeService {

  static readonly REGEX_NODE_DIR = /^node\:dir\:\S+/;
  static readonly REGEX_NODE_FILE = /^node\:file\:\S+/;

  private selected: TreeNode[] = [];
  private selectedSubject = new BehaviorSubject<TreeNode[]>([]);
  readonly selected$ = this.selectedSubject.asObservable();

  static assemblyTree([rawDirs, rawFiles]) {

    const dirs = _.sortBy( rawDirs.map(Directory.fromJSON), 'name');
    const files = _.sortBy( rawFiles.map(File.fromJSON), 'name');

    const root: TreeNode[] = [];

    dirs.forEach(dir => {
      const parentDir = dirs.find(otherDir => otherDir.id === dir.parentId);
      if (!!parentDir) {
        parentDir.addChild(dir);
      } else {
        root.push(dir);
      }
    });

    files.forEach(file => {
      const parentDir = dirs.find(dir => dir.id === file.parentId);
      if (!!parentDir) {
        parentDir.addChild(file);
      } else {
        root.push(file);
      }
    });
    return Promise.all([root, dirs, files]);
  }

  constructor(private localStorage: LocalStorageService) {
    this.loadTree();
  }

  loadTree(): Observable<TreeNode[]> {
    const loading = Promise
      .all([this.loadDirs(), this.loadFiles()])
      .then( TreeService.assemblyTree )
      .then(([root, dirs, files]) => {

        this.selected = dirs.concat(files).filter(node => node.checked);
        this.emitSelected();

        return Promise.resolve(root);
      });

    return Observable.fromPromise(loading);
  }

  replaceNode(nodeId: string, node: TreeNode): Observable<boolean> {
    try {
      const dto = node.toJSON();
      this.localStorage.set(nodeId, JSON.stringify(dto));
      return Observable.of(true);
    } catch (err) {
      return Observable.throw(err);
    }
  }

  removeBranch(nodeId: string): Observable<boolean> {
    const accumulator: string[] = [];
    this.queryIdsToRemove(nodeId, accumulator);
    this.localStorage.remove(...accumulator);

    this.reshapeTree(accumulator);

    return Observable.of(true);
  }
  removeNodes(selectedIds: string[]): Observable<boolean> {
    const removingTasks = selectedIds.map( nodeId =>  this.removeBranch( nodeId ) );
    return Observable.merge(...removingTasks);
  }

  createDirectory( directory: Directory ): Observable<Directory> {

    let parentId = directory.parentId;
    if ( !this.isNodeExists(parentId) ) {
      // To the root
      parentId = null;
    }

    try {
      const newId = `node:dir:${nanoId()}`;

      const { name, children, expanded, checked } = directory;
      const pseudoSavedDirectory = new Directory(newId, name, children, parentId, expanded, checked );
      const data = JSON.stringify( pseudoSavedDirectory.toJSON() );

      this.localStorage.set(newId, data);

      return Observable.of(pseudoSavedDirectory);
    } catch (err) {
      return Observable.throw(err);
    }
  }

  createFile<K, V>(file: File, contentObj: FileContent<K, V>): Observable<File> {

    let parentId = file.parentId;
    if ( !this.isNodeExists(parentId) ) {
      // To the root
      parentId = null;
    }
    // TODO - transaction like processing

    try {
      const newFileId = `node:file:${nanoId()}`;

      const { name, price} = file;
      const pseudoSavedFile = new File(newFileId, name, price, null, parentId);


      return this
        .createContent<K, V>(contentObj)
        .switchMap( content => {

          pseudoSavedFile.content = content.id.toString();
          const data = JSON.stringify( pseudoSavedFile.toJSON() );

          this.localStorage.set(newFileId, data);

          return Observable.of(pseudoSavedFile);
        });
    } catch ( err ) {
      return Observable.throw(err);
    }
  }

  createContent<K, V>( contentObj: FileContent<K, V>): Observable<FileContent<K, V>> {
    try {

      const newContentId = `content:${nanoId()}` as any;
      const { content } = contentObj;

      const pseudoSavedContent = new FileContent<K, V>(newContentId, content);
      const data = JSON.stringify( pseudoSavedContent );

      this.localStorage.set(newContentId, data);
      return Observable.of(pseudoSavedContent);

    } catch ( err ) {
      return Observable.throw(err);
    }
  }

  populateTree(): Observable<TreeNode[]> {
    const cleanUpAndInsert = new Promise((resolve, reject) => {

      this.localStorage.clearAll();

      MOCK_DATA.FILE_CONTENTS.forEach(content => {
        try {
          this.localStorage.set(
            content.id, JSON.stringify(content)
          );
        } catch (ignore) {
        }
      });

      MOCK_DATA.FILES.forEach(node => {
        try {
          this.localStorage.set(node.id, JSON.stringify(node.toJSON()));
        } catch (ignore) {
        }
      });

      MOCK_DATA.DIRS.forEach(node => {
        try {
          this.localStorage.set(node.id, JSON.stringify(node.toJSON()));
        } catch (ignore) {
        }
      });

      resolve();
    });

    return Observable.fromPromise(cleanUpAndInsert)
      .switchMap(() => this.loadTree());
  }

  getNodeContent(nodeId: string): Observable<any> {
    try {
      const parentNodeRaw: any = this.localStorage.get(nodeId);

      if (!parentNodeRaw) {
        return Observable.of();
      }

      const parentNodeObj = JSON.parse(parentNodeRaw);
      const {content} = parentNodeObj;

      if (!parentNodeRaw  || !content) {
        return Observable.of();
      }

      const contentRaw: any = this.localStorage.get(content);
      const contentObj = JSON.parse(contentRaw);

      if (!contentRaw || !contentObj) {
        return Observable.of();
      }

      return Observable.of(contentObj);
    } catch (err) {
      return Observable.throw(err);
    }
  }

  checked(node: TreeNode) {
    this.replaceNode(node.id, node)
      .toPromise()
      .then(() => this.emitSelected(node));
  }

  getSelected(): TreeNode[] {
    return this.selectedSubject.getValue().slice();
  }

  /**
   * Clean up all staying nodes of removed descendants
   *
   * @param {string[]} removedNodes - ids of removed nodes
   */
  private reshapeTree(removedNodes: string[]) {
    this
      .queryKeys(TreeService.REGEX_NODE_DIR)
      .then(keys => {
        keys.forEach(key => {
          this.removeChildrenFor( key, removedNodes );
        });
      });
  }

  private queryIdsToRemove(id: string, delAccum: string[]): void {

    const node = JSON.parse(this.localStorage.get(id));

    // Node not exists
    if ( !node ) {
      return;
    }

    delAccum.push(node.id);

    // Related content records
    if (!!node.content) {
      delAccum.push(node.content);
    }
    // Queryng down to leafs
    if (!!node.children && node.children.length > 0) {
      node.children.forEach(nId => this.queryIdsToRemove(nId, delAccum));
    } else {
      return;
    }
  }

  private queryKeys(keyRegEx: RegExp): Promise<string[]> {
    return new Promise((resolve, reject) => {
      try {
        const keys = this.localStorage.keys().filter(key => key.match(keyRegEx));
        resolve(keys);
      } catch (err) {
        reject(err);
      }
    });
  }

  private loadDirs(): Promise<any[]> {
    const retriever = this.localStorage.get.bind(this.localStorage);
    const parser = JSON.parse.bind(JSON);

    return this.queryKeys(TreeService.REGEX_NODE_DIR)
      .then(keys => keys.map(retriever).map(parser));
  }

  private loadFiles(): Promise<any[]> {

    const retriever = this.localStorage.get.bind(this.localStorage);
    const parser = JSON.parse.bind(JSON);

    return this.queryKeys( TreeService.REGEX_NODE_FILE)
      .then(keys => keys.map(retriever).map(parser));
  }

  private emitSelected(...nodes: TreeNode[]) {
    if (nodes) {
      this.selected.push(...nodes);
    }
    this.selected = _.filter(this.selected, { checked: true });
    this.selectedSubject.next(this.selected.slice());
  }

  private removeChildrenFor(parentNodeId: string, removedChildrenId: string[]): void {

    const nodeRaw: any = this.localStorage.get(parentNodeId);
    const node = JSON.parse(nodeRaw);

    // No such parent
    if (!node) {
      return;
    }

    node.children = _.difference(node.children, removedChildrenId);
    this.localStorage.set(node.id, JSON.stringify(node));

  }

  private isNodeExists(parentId: string): boolean {
    return !!this.localStorage.get(parentId);
  }
}
