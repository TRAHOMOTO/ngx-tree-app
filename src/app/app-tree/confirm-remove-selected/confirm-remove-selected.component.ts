import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/merge';

import {TreeService} from '../services/tree.service';
import {TreeNode} from '../model/tree-node';

@Component({
  selector: 'app-confirm-remove-selected',
  templateUrl: './confirm-remove-selected.component.html',
  styleUrls: ['./confirm-remove-selected.component.css']
})
export class ConfirmRemoveSelectedComponent implements OnInit, OnDestroy {

  private subscr: Subscription;
  nodes: TreeNode[];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private treeService: TreeService) { }

  ngOnDestroy() {
    if (this.subscr) {
      this.subscr.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.nodes = this.treeService.getSelected();

    if ( !this.nodes.length ) {
      this.router.navigateByUrl('tree');
    }

  }

  confirmed() {
    const selectedIds = this.nodes.map( n => n.id );

    this.treeService
      .removeNodes( selectedIds )
      .subscribe(
        () => {}, // can be progress added
        console.error, // TODO - redirect to ErrorPage
        () => this.router.navigateByUrl('tree')
      );
  }

}
