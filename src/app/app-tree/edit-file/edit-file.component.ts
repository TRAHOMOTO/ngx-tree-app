import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TreeService} from '../services/tree.service';
import {File} from '../model/file';
import {FileContent} from '../model/file-content';

// TODO - move part of functionality to parent component. E.g. create smart parent

@Component({
  selector: 'app-edit-file',
  templateUrl: './edit-file.component.html'
})
export class EditFileComponent implements OnInit {

  fileEditForm: FormGroup;
  parentId: string;

  static validatorNotLessZero(control: AbstractControl): any {
    const value = Number(control.value);

    if ( isNaN(value) ||  control.value < 0) {
      return {
        notLessZero: 'Price can not be negative'
      };
    }

    return null;
  }

  constructor(private router: Router,
              private route: ActivatedRoute,
              private treeService: TreeService) {
  }

  ngOnInit() {
    this.parentId = this.route.snapshot.params['parentId'];

    if (!this.parentId) {
      this.router.navigateByUrl('/');
    }

    this.createForm();
  }

  private createForm() {
    this.fileEditForm = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      price: new FormControl(null, [ Validators.required, EditFileComponent.validatorNotLessZero ]),
      content: new FormControl(null, [Validators.required])
    });
  }

  onSubmit() {
    if (!this.isFormValid) {
      return;
    }

    const { name, price, content } = this.fileEditForm.value;
    const priceInCents = price * 100;

    const contentObj = new FileContent<string, string>(null, content);
    const file = new File( null, name, priceInCents, null, this.parentId);

    this.treeService
      .createFile<string, string>(file, contentObj)
      .subscribe(
        (f) => this.router.navigateByUrl('/'),
        err => alert(err.message) , // TODO - redirect to Error page
      );
  }

  get nameControl(): AbstractControl {
    return this.fileEditForm.get('name');
  }

  get priceControl(): AbstractControl {
    return this.fileEditForm.get('price');
  }

  get contentControl(): AbstractControl {
    return this.fileEditForm.get('content');
  }

  get isFormValid(): boolean {
    return this.fileEditForm.valid;
  }

  isValid(control: AbstractControl): boolean {
    return control.valid && (control.dirty || control.touched);
  }

  isInvalid(control: AbstractControl): boolean {
    return control.invalid && (control.dirty || control.touched);
  }

  hasError(control: AbstractControl, errName: string): any {
    return control.errors[errName];
  }
}
