import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TreeHomeComponent} from './tree-home/tree-home.component';
import {ConfirmComponent} from './confirm/confirm.component';
import {FileContentComponent} from './file-content/file-content.component';
import {CheckoutComponent} from './checkout/checkout.component';
import {PayMeResolver} from './services/pay-me.resolver';
import {ConfirmRemoveSelectedComponent} from './confirm-remove-selected/confirm-remove-selected.component';
import {EditDirectoryComponent} from './edit-directory/edit-directory.component';
import {EditFileComponent} from './edit-file/edit-file.component';

const routes: Routes = [
  // TODO - add guards for checking params
  {
    path: 'tree',
    component: TreeHomeComponent,
    children: [
      { path: 'details/:id', component: FileContentComponent, outlet: 'details', pathMatch: 'full' },
    ]
  },
  { path: 'confirm/:id', component: ConfirmComponent },
  { path: 'pay', component:  CheckoutComponent, resolve: { pay: PayMeResolver } },
  { path: 'remove-selected/:ids', component: ConfirmRemoveSelectedComponent },
  {
    path: 'add',
    children: [
      { path: 'directory/:parentId', component: EditDirectoryComponent },
      { path: 'file/:parentId', component: EditFileComponent }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppTreeRoutingModule { }
