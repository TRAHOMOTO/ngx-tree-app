import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/pluck';

import {FileContent} from '../model/file-content';
import {TreeService} from '../services/tree.service';

@Component({
  selector: 'app-file-content',
  templateUrl: './file-content.component.html'
})
export class FileContentComponent implements OnInit {

  content$: Observable<FileContent<string, string>>;
  constructor(private route: ActivatedRoute,
              private treeService: TreeService) { }

  ngOnInit() {
    this.content$ = this.route.params
      .pluck('id')
      .switchMap( (id: string) => this.treeService.getNodeContent(id) );
  }

}
