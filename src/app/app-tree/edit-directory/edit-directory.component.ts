import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TreeService} from '../services/tree.service';
import {Directory} from '../model/directory';

// TODO - move part of functionality to parent component. E.g. create smart parent

@Component({
  selector: 'app-edit-directory',
  templateUrl: './edit-directory.component.html'
})
export class EditDirectoryComponent implements OnInit {

  dirEditForm: FormGroup;
  parentId: string;

  constructor( private router: Router,
               private route: ActivatedRoute,
               private treeService: TreeService ) {}

  ngOnInit() {
    this.parentId = this.route.snapshot.params['parentId'];

    if (!this.parentId) {
      this.router.navigateByUrl('/');
    }

    this.createForm();
  }

  private createForm() {
    this.dirEditForm = new FormGroup({
      name: new FormControl(null, [Validators.required] )
    });
  }

  onSubmit() {
    if (!this.isFormValid) {
      return;
    }

    const { name } = this.dirEditForm.value;
    const directory = new Directory( null, name, [], this.parentId );

    this.treeService
      .createDirectory(directory)
      .subscribe(
        () => this.router.navigateByUrl('/'),
        err => alert(err.message) , // TODO - redirect to Error page
      );
  }

  get nameControl(): AbstractControl {
    return this.dirEditForm.get('name');
  }

  get isFormValid(): boolean {
    return this.dirEditForm.valid;
  }

  isValid(control: AbstractControl): boolean {
    return control.valid && (control.dirty || control.touched);
  }
  isInvalid(control: AbstractControl): boolean {
    return control.invalid && (control.dirty || control.touched)
  }
  hasError(control: AbstractControl, errName: string): any {
    return control.errors[errName];
  }
}
