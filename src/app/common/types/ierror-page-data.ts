export interface IErrorPageData {
  title: string;
  description: string;
}
