import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {IErrorPageData} from '../types/ierror-page-data';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.css']
})
export class ErrorPageComponent implements OnInit {

  data$: Observable<IErrorPageData>;

  constructor(private router: ActivatedRoute) { }

  ngOnInit() {
    this.data$ = this.router.data as Observable<IErrorPageData>;
  }

}
